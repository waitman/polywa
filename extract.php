<?php

/* You can use this to unzip downloaded archive from Bandcamp. 
 * it creates the destination directory using the filename of 
 * the archive, then moves the zip into a "zips" directory
 * (create this first!)
 */

$fn = $argv[1];

$dir = str_replace('.zip','',$fn);
mkdir($dir);
system('unzip "'.$fn.'" -d "'.$dir.'"');
system('mv "'.$fn.'" zips');
echo 'ok'."\n";
exit();

