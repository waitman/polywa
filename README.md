== polywa.cpp ==

This GTK3 audio player program creates a visual menu of audio files
in WAV format, which are in "albums" organized by directory path.
The cover images are supposed to be "cover.jpg" in each directory.
Clicking an album cover or title plays the audio through the 
default soundcard. 

This is intended to be used with downloaded Bandcamp digital albums
in WAV format. Will add flac, aiff, mp3 later.

Contact Waitman Gobble <waitman@waitman.net> or join us in the #polywa
room on https://wubyo.cc/#/room/#polywa:www.wubyo.cc

```
Instructions: set MUSICPATH, SCREENWIDTH, SCREENHEIGHT
You may change CELLWIDTH and CELLHEIGHT to your liking.
build on Linux: type 'make'
build on FreeBSD: type 'make freebsd'
clean: type 'make clean'
run: type 'make run'
no install, yet.
requirements: gtk+-3.0, gcc/clang build, libstdc++fs, libglib2.0
```
