all: polywa

polywa: 
	c++ -o polywa polywa.cpp -I/usr/include -L/usr/lib \
		`pkg-config --cflags --libs gtk+-3.0` \
		`pkg-config --cflags --libs glib-2.0` \
		-lm -ldl -lstdc++fs -pthread -rdynamic -Wl,--export-dynamic -Wall

freebsd:
	c++ -o polywa polywa.cpp -I/usr/local/include -L/usr/local/lib \
		`pkg-config --cflags --libs gtk+-3.0` \
		-pthread -rdynamic -Wl,--export-dynamic -Wall

clean:
	rm -f polywa
	rm -f *.core

run:
	./polywa &
