/*
 * polywa.cpp
 * This GTK3 audio player program creates a visual menu of audio files
 * in WAV format, which are in "albums" organized by directory path.
 * The cover images are supposed to be "cover.jpg" in each directory.
 * Clicking an album cover or title plays the audio through the 
 * default soundcard. 
 * This is intended to be used with downloaded Bandcamp digital albums
 * in WAV format. Will add flac, aiff, mp3 later.
 * Contact Waitman Gobble <waitman@waitman.net> or join us in the 
 * #polywa room on https://wubyo.cc/#/room/#polywa:www.wubyo.cc
 * 
 * Instructions: set MUSICPATH, SCREENWIDTH, SCREENHEIGHT
 * You may change CELLWIDTH and CELLHEIGHT to your liking.
 * build on Linux: type 'make'
 * build on FreeBSD: type 'make freebsd'
 * clean: type 'make clean'
 * run: type 'make run'
 * no install, yet.
 * requirements: gtk+-3.0, gcc/clang build, libstdc++fs, libglib2.0
 */
 
#include <glib.h>
#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <cmath>
#include <string>
#include <algorithm>
#include <vector>
#include <cstring>
#include <experimental/filesystem>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define MUSICPATH "/home/waitman/drive/Bandcamp/"
#define MINIAUDIO_IMPLEMENTATION
#include "miniaudio.h"

#define SCREENWIDTH 1920
#define SCREENHEIGHT 1080
#define CELLWIDTH 310
#define CELLHEIGHT 360
#define MAXLOAD 512

using namespace std;

typedef struct {
    GtkWidget *Box1;
    GtkWidget *Box2;
    GtkWidget *Box3;
    GtkWidget *PauseButton;
    GtkWidget *StopButton;
    GtkWidget *NextButton;
    GtkWidget *PrevButton;
    GtkWidget *Draw1;
    GtkWidget *Menu1;
    GtkWidget *Grid1;
} app_widgets;

GtkWidget 	*MainLabel;
GtkWidget	*window;
GtkWidget	*ev[MAXLOAD];
GtkWidget	*bx[MAXLOAD];
GtkWidget	*img[MAXLOAD];
GtkWidget	*lbls[MAXLOAD];
GtkWidget	*evl[MAXLOAD];
int		num_tracks[MAXLOAD];
gchar		*labels[MAXLOAD];
std::string	WhatsPlaying;
std::string	musicpath;

gboolean	isPlaying = FALSE;
gboolean	isPaused = FALSE;
gboolean	doAdvance = FALSE;
gboolean	doBack = FALSE;
int		loadNext = -1;
int		lastPlay = -1;

int		grid_cols = 3;
gboolean	is_paused = FALSE;
gboolean	is_done = FALSE;
int		PlayListIndex = 0;
int		secplay = 0;
std::vector<std::string> PlayList; 
std::vector<std::string> Catalog;

ma_result	result;
ma_decoder	decoder;
ma_device_config deviceConfig;
ma_device	device;

struct WAVEHEADER {
	unsigned char riff[4];						
	unsigned int overall_size;				
	unsigned char wave[4];
	unsigned char fmt_chunk_marker[4];
	unsigned int length_of_fmt;
	unsigned int format_type;
	unsigned int channels;
	unsigned int sample_rate;
	unsigned int byterate;
	unsigned int block_align;
	unsigned int bits_per_sample;
	unsigned char data_chunk_header[4];
	unsigned int data_size;
};

namespace
{
    struct case_insensitive_less : public std::binary_function< char,char,bool >
    {
        bool operator () (char x, char y) const
        {
            return toupper( static_cast< unsigned char >(x)) < 
                   toupper( static_cast< unsigned char >(y));
        }
    };
    bool NCL(const std::string &a, const std::string &b)
    {
        return std::lexicographical_compare( a.begin(),a.end(),
                b.begin(),b.end(), case_insensitive_less() );
    }
}

void parsewave(std::string fn)
{
	unsigned char bufq[4];
	unsigned char bufd[2];

	FILE *wav;
	struct WAVEHEADER header;
	
	wav = fopen(fn.c_str(), "r");
	if (wav == NULL) {
		printf("Error opening file\n");
		exit(1);
	}
	
	fread(header.riff, sizeof(header.riff), 1, wav);
	fread(bufq, sizeof(bufq), 1, wav);
	header.overall_size  = bufq[0] | (bufq[1]<<8) | 
		(bufq[2]<<16) | (bufq[3]<<24);
	fread(header.wave, sizeof(header.wave), 1, wav);
	fread(header.fmt_chunk_marker, sizeof(header.fmt_chunk_marker), 1, wav);
	fread(bufq, sizeof(bufq), 1, wav);
	header.length_of_fmt = bufq[0] | (bufq[1] << 8) |
							(bufq[2] << 16) | (bufq[3] << 24);
	fread(bufd, sizeof(bufd), 1, wav);
	header.format_type = bufd[0] | (bufd[1] << 8);
	fread(bufd, sizeof(bufd), 1, wav);
	header.channels = bufd[0] | (bufd[1] << 8);
	fread(bufq, sizeof(bufq), 1, wav);
	header.sample_rate = bufq[0] | (bufq[1] << 8) |
						(bufq[2] << 16) |(bufq[3] << 24);
	fread(bufq, sizeof(bufq), 1, wav);
	header.byterate  = bufq[0] | (bufq[1] << 8) |
						(bufq[2] << 16) | (bufq[3] << 24);
	float duration = (float) header.overall_size / header.byterate;
	secplay = ceil(duration)+1;

	fclose(wav);
	return;
}

void data_callback(ma_device* pDevice, void* pOutput, const void* pInput, ma_uint32 frameCount)
{
    ma_decoder* pDecoder = (ma_decoder*)pDevice->pUserData;
    if (pDecoder == NULL) {
		is_done = TRUE;
        return;
    }

    ma_uint64 framesRead = ma_decoder_read_pcm_frames(pDecoder, pOutput, frameCount);
    if (framesRead < frameCount) {
        doAdvance = TRUE;
    } else {
		isPlaying = TRUE;
	}

    (void)pInput;
}

void pause_device()
{
	if (isPlaying)
	{
	    if (ma_device_stop(&device) != MA_SUCCESS)
	    {
			printf("Failed to stop playback device.\n");
			ma_device_uninit(&device);
			ma_decoder_uninit(&decoder);
		}
    }
}

void restart_device()
{
	if (isPlaying)
	{
	    if (ma_device_start(&device) != MA_SUCCESS)
	    {
			printf("Failed to start playback device.\n");
			ma_device_uninit(&device);
			ma_decoder_uninit(&decoder);
		}
	}
}

int enqueue(std::string fn)
{
    result = ma_decoder_init_file(fn.c_str(), NULL, &decoder);
    if (result != MA_SUCCESS) {
        return -2;
    }

    deviceConfig = ma_device_config_init(ma_device_type_playback);
    deviceConfig.playback.format   = decoder.outputFormat;
    deviceConfig.playback.channels = decoder.outputChannels;
    deviceConfig.sampleRate        = decoder.outputSampleRate;
    deviceConfig.dataCallback      = data_callback;
    deviceConfig.pUserData         = &decoder;

    if (ma_device_init(NULL, &deviceConfig, &device) != MA_SUCCESS) {
        printf("Failed to open playback device.\n");
        ma_decoder_uninit(&decoder);
        return -3;
    }
	
    if (ma_device_start(&device) != MA_SUCCESS) {
        printf("Failed to start playback device.\n");
        ma_device_uninit(&device);
        ma_decoder_uninit(&decoder);
        return -4;
    }
    
    parsewave(fn);
    return 0;
}

void encode(std::string& data) {
    std::string buffer;
    buffer.reserve(data.size());
    for(size_t pos = 0; pos != data.size(); ++pos) {
        switch(data[pos]) {
            case '&':  buffer.append("&amp;");       break;
            case '\"': buffer.append("&quot;");      break;
            case '\'': buffer.append("&apos;");      break;
            case '<':  buffer.append("&lt;");        break;
            case '>':  buffer.append("&gt;");        break;
            default: 
					if ((data[pos]>31)&&(data[pos]<127))
						buffer.append(&data[pos], 1);
					break;
        }
    }
    data.swap(buffer);
}

void do_pause()
{
	if (isPaused)
	{
		restart_device();
		isPaused = FALSE;
	} else {
		pause_device();
		isPaused = TRUE;
	}
}

void do_next()
{
	doAdvance = TRUE;
}

void do_prev()
{
	doBack = TRUE;
}

void stop_music()
{
	if (isPlaying)
	{
		ma_device_uninit(&device);
		ma_decoder_uninit(&decoder);
		isPlaying = false;
	}
}	
void do_stop()
{
	stop_music();
	WhatsPlaying="";
	secplay = 0;
}

static void label_click(GtkWidget *widget, gpointer ud)
{
	int i = atoi(gtk_widget_get_tooltip_text(widget));	
	const gchar *label_text;
	std::string pg;
	std::string pa;
	std::size_t found;
	
	PlayListIndex = 0;
	PlayList.clear();
	if ((i>=0)&&(i<MAXLOAD))
	{
		label_text = labels[i];
		std::string xpg;
		gchar		*markup;

		if (lastPlay>-1)
		{
			xpg = labels[lastPlay];
			xpg = xpg.substr(musicpath.length(),xpg.length()-musicpath.length()-10);
			encode(xpg);
			markup = g_strdup_printf("<span foreground='turquoise'>%s</span>",xpg.c_str());
			gtk_label_set_markup(GTK_LABEL(lbls[lastPlay]),markup);
		}
		xpg = labels[i];
		xpg = xpg.substr(musicpath.length(),xpg.length()-musicpath.length()-10);
		lastPlay = i;
		encode(xpg);
		markup = g_strdup_printf("<span foreground='pink'><b>%s</b></span>",xpg.c_str());
		gtk_label_set_markup(GTK_LABEL(lbls[i]),markup);

		pg = label_text;
		pg = pg.substr(0,pg.length()-10);
	
		for(auto& p: std::experimental::filesystem::recursive_directory_iterator(pg))
		{
			pa = p.path();
			found=pa.find(".wav");
			if (found!=std::string::npos)
			{
				PlayList.push_back(pa);
			}
		}
	}
	if (PlayList.size()>0)
	{
		std::sort(PlayList.begin(), PlayList.end(),NCL);
		gtk_main_iteration_do(FALSE);
		stop_music();
		loadNext=2;
		WhatsPlaying = PlayList[0];
	}
}

static gboolean
_label_update(gpointer data)
{
    GtkLabel *label = (GtkLabel*)data;
    
    if (loadNext==0)
    {
		loadNext=-1;
		enqueue(PlayList[PlayListIndex]);
		WhatsPlaying = PlayList[PlayListIndex];
	}
	
	if (loadNext>0)
	{
		loadNext--;
	}
	
	if (doBack)
	{
		doBack = FALSE;
		stop_music();
		PlayListIndex--;
		if (PlayListIndex<0) PlayListIndex=0;
		loadNext = 3;
	}
	
    if (doAdvance)
    {
		doAdvance = FALSE;
		stop_music();
		PlayListIndex++;
		if (PlayListIndex<(int)PlayList.size())
		{
			WhatsPlaying = PlayList[PlayListIndex];
			loadNext = 3;
		}
	}
			
    if (WhatsPlaying.length() > musicpath.length())
    {
		std::string tmpname = WhatsPlaying;
		if (!isPaused) secplay--;
		if (secplay<0) secplay = 0;
		tmpname  = tmpname.substr(musicpath.length(),tmpname.length()-musicpath.length());
		encode(tmpname);
		gchar		*markup;
		if (isPaused)
		{
			markup = g_strdup_printf("<span foreground='pink'><b>Now Playing:</b> %s</span>\n<span foreground='grey'>[%i/%i] %i PAUSED</span>",tmpname.c_str(),(PlayListIndex+1),(int)PlayList.size(),secplay);
		} else {
			markup = g_strdup_printf("<span foreground='pink'><b>Now Playing:</b> %s</span>\n<span foreground='grey'>[%i/%i] %i</span>",tmpname.c_str(),(PlayListIndex+1),(int)PlayList.size(),secplay);	
		}
		gtk_label_set_markup(label, markup);
	} else {
		gchar		*markup;
		markup = g_strdup_printf("<span foreground='pink'><b>Nothing Is Playing</b></span>");
		gtk_label_set_markup(label, markup);
	}
	return TRUE;

}

int main(int argc, char *argv[])
{
	GtkBuilder	*builder; 
	app_widgets	*widgets = g_slice_new(app_widgets);
	GdkPixbuf	*pixbuf = NULL;
	GdkPixbuf	*pxbscaled = NULL;
	gchar		*markup;
	std::size_t found;
	WhatsPlaying = "";
	musicpath = MUSICPATH;

	int j = 0;
	
	for(auto& p: std::experimental::filesystem::recursive_directory_iterator(musicpath))
	{
		std::string pg = p.path();
		found=pg.find("/cover.jpg");
		if (found!=std::string::npos)
		{
			Catalog.push_back(pg);
		}
	}
	std::sort(Catalog.begin(), Catalog.end(),NCL);
	for (int i=0;i<(int)Catalog.size();i++)
	{
		labels[j++] = g_strdup_printf("%s",Catalog[i].c_str());
	}
	gtk_init(&argc, &argv);

	builder = gtk_builder_new_from_file("glade/polywa.glade");

	window = GTK_WIDGET(gtk_builder_get_object(builder, "Window1"));
	g_signal_connect (G_OBJECT (window), "destroy", 
                    G_CALLBACK (gtk_main_quit),
                    NULL);
	widgets->Box1 = GTK_WIDGET(gtk_builder_get_object(builder, "Box1"));
	widgets->Box2 = GTK_WIDGET(gtk_builder_get_object(builder, "Box2"));
	widgets->Box3 = GTK_WIDGET(gtk_builder_get_object(builder, "Box3"));
	widgets->Menu1 = GTK_WIDGET(gtk_builder_get_object(builder, "Menu1"));
	widgets->Grid1 = GTK_WIDGET(gtk_builder_get_object(builder, "Grid1"));
	widgets->Draw1 = GTK_WIDGET(gtk_builder_get_object(builder, "Draw1"));
	widgets->PauseButton = GTK_WIDGET(gtk_builder_get_object(builder, "PauseButton"));
	widgets->StopButton = GTK_WIDGET(gtk_builder_get_object(builder, "StopButton"));
	widgets->NextButton = GTK_WIDGET(gtk_builder_get_object(builder, "NextButton"));
	widgets->PrevButton = GTK_WIDGET(gtk_builder_get_object(builder, "PrevButton"));
	
	g_signal_connect(G_OBJECT(widgets->NextButton), "clicked", G_CALLBACK(do_next), MainLabel);
	g_signal_connect(G_OBJECT(widgets->PrevButton), "clicked", G_CALLBACK(do_prev), MainLabel);
	g_signal_connect(G_OBJECT(widgets->PauseButton), "clicked", G_CALLBACK(do_pause), MainLabel);
	g_signal_connect(G_OBJECT(widgets->StopButton), "clicked", G_CALLBACK(do_stop), MainLabel);
	MainLabel = GTK_WIDGET(gtk_builder_get_object(builder, "MainLabel"));
	gtk_label_set_line_wrap(GTK_LABEL(MainLabel),TRUE);

  	int o_width, o_height,n_width,n_height;
	double scale;
	int gx = 0;
	int gy = 0;
	
	grid_cols = floor(SCREENWIDTH/CELLWIDTH);
	
	for (int i=0;i<j;i++)
	{
		ev[i] = gtk_event_box_new();
		evl[i] = gtk_event_box_new();
		bx[i] = gtk_box_new(GTK_ORIENTATION_VERTICAL, 2);
		img[i] = gtk_image_new();

		pixbuf = gdk_pixbuf_new_from_file(labels[i], NULL);

		std::string pg = labels[i];
		found=pg.find("/cover.jpg");
		if (found!=std::string::npos)
		{
			pg  = pg.substr(musicpath.length(),pg.length()-musicpath.length()-10);
			lbls[i] = gtk_label_new("");
			gtk_widget_set_size_request(lbls[i],CELLWIDTH,CELLHEIGHT-CELLWIDTH);
			encode(pg);
			markup = g_strdup_printf("<span foreground='turquoise'>%s</span>",pg.c_str());
			gtk_label_set_line_wrap(GTK_LABEL(lbls[i]),TRUE);
			gtk_label_set_markup(GTK_LABEL(lbls[i]),markup);
		}
				
		char buffer[10];
		sprintf(buffer, "%d", i);
		gtk_widget_set_tooltip_text(ev[i],buffer);
		gtk_widget_set_tooltip_text(evl[i],buffer);
		
		o_width = gdk_pixbuf_get_width(pixbuf);
		o_height = gdk_pixbuf_get_height(pixbuf);
		n_width = 0;
		n_height = 0;
		
		if (o_width>o_height)
		{
			scale = (double)CELLWIDTH/(double)o_width;
			n_width = (int)CELLWIDTH;
			n_height = (int)ceil(scale * (double)o_height);
		} else {
			scale = (double)CELLWIDTH/(double)o_height;
			n_height = (int)CELLWIDTH;
			n_width = (int)ceil(scale * (double)o_width);
		}
		
		pxbscaled = gdk_pixbuf_scale_simple(pixbuf, n_width, n_height, GDK_INTERP_BILINEAR);
		gtk_image_set_from_pixbuf(GTK_IMAGE(img[i]), pxbscaled);
		
		gtk_widget_set_size_request(ev[i],CELLWIDTH,CELLWIDTH);
		gtk_container_add(GTK_CONTAINER(ev[i]),img[i]);
		gtk_widget_show(img[i]);
		
		gtk_widget_set_size_request(bx[i],CELLWIDTH,CELLHEIGHT);
		gtk_box_pack_start(GTK_BOX(bx[i]), ev[i], TRUE, TRUE, 3);
		
		gtk_widget_set_size_request(evl[i],CELLWIDTH,CELLHEIGHT-CELLWIDTH);
		gtk_container_add(GTK_CONTAINER(evl[i]),lbls[i]);
		gtk_label_set_xalign(GTK_LABEL(lbls[i]),0);
		gtk_label_set_yalign(GTK_LABEL(lbls[i]),0);
		gtk_widget_show(lbls[i]);

		gtk_box_pack_end(GTK_BOX(bx[i]), evl[i], TRUE, TRUE, 3);
		
		gtk_widget_show(ev[i]);
		gtk_widget_show(evl[i]);
		gtk_widget_show(bx[i]);
		
		g_signal_connect(GTK_EVENT_BOX(ev[i]),"button_press_event", G_CALLBACK(label_click), GINT_TO_POINTER(i));
		g_signal_connect(GTK_EVENT_BOX(evl[i]),"button_press_event", G_CALLBACK(label_click), GINT_TO_POINTER(i));
		gtk_grid_attach(GTK_GRID(widgets->Grid1),bx[i],gx,gy,1,1);
		gx++;
		if (gx==grid_cols)
		{
			gx=0;
			gy++;
		}
	}
	GtkCssProvider *provider = gtk_css_provider_new ();
	gtk_css_provider_load_from_path (provider, "css/gtk-widgets.css", NULL);

	GtkStyleContext *context;
	context = gtk_widget_get_style_context(window);
	gtk_style_context_add_provider (context,
                                    GTK_STYLE_PROVIDER(provider),
                                    GTK_STYLE_PROVIDER_PRIORITY_USER);

	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(window), SCREENWIDTH, SCREENHEIGHT);

	gtk_builder_connect_signals(builder, widgets);
	
	g_object_unref(builder);
   
	gtk_widget_show(window);
	
	
	g_timeout_add_seconds(1, _label_update, MainLabel);

	gtk_main();
	g_slice_free(app_widgets, widgets);

	return 0;
}

extern "C" void on_window_main_delete_event()
{
    gtk_main_quit();
}

extern "C" void on_window_main_destroy()
{
    gtk_main_quit();
}

